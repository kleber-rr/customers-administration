package com.kleber.webservices.customersadministration.service;

import java.util.List;
import java.util.Optional;

import com.kleber.webservices.customersadministration.bean.Customer;
import com.kleber.webservices.customersadministration.helper.Status;
import com.kleber.webservices.customersadministration.repository.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerDetailService {

    @Autowired
    private CustomerRepository repository;

    public Customer findById(Integer id){
        Optional<Customer> customerOptional = repository.findById(id);
        if(customerOptional.isPresent()){
            return customerOptional.get();
        }

        return null;
    }

    public List<Customer> findAll(){
        return repository.findAll();
    }

    public Status deleteById(Integer id){
        try{
            repository.deleteById(id);
            return Status.SUCCESS;
        } catch(Exception e){
            return Status.FAILURE;
        }        
    }

    public Status insert(Customer customer){
        try{
            repository.save(customer);
            return Status.SUCCESS;
        } catch(Exception e){
            return Status.FAILURE;
        }   
    }
}