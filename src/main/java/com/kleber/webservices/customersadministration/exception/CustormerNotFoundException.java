package com.kleber.webservices.customersadministration.exception;

import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.CUSTOM, customFaultCode = "{http://klebercardoso.com}001_Customer_Not_Found")
public class CustormerNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CustormerNotFoundException(String message) {
        super(message);
    }
    

    
}