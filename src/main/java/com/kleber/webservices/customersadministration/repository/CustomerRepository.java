package com.kleber.webservices.customersadministration.repository;

import com.kleber.webservices.customersadministration.bean.Customer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{

    

}