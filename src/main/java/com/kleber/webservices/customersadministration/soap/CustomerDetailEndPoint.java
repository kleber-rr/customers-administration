package com.kleber.webservices.customersadministration.soap;

import java.util.List;

import com.kleber.webservices.customersadministration.bean.Customer;
import com.kleber.webservices.customersadministration.exception.CustormerNotFoundException;
import com.kleber.webservices.customersadministration.service.CustomerDetailService;
import com.klebercardoso.CustomerDetail;
import com.klebercardoso.DeleteCustomerRequest;
import com.klebercardoso.DeleteCustomerResponse;
import com.klebercardoso.GetAllCustomerDetailRequest;
import com.klebercardoso.GetAllCustomerDetailResponse;
import com.klebercardoso.GetCustomerDetailRequest;
import com.klebercardoso.GetCustomerDetailResponse;
import com.klebercardoso.InsertCustomerRequest;
import com.klebercardoso.InsertCustomerResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class CustomerDetailEndPoint {

    @Autowired
    CustomerDetailService service;

    @PayloadRoot(namespace = "http://klebercardoso.com", localPart = "GetCustomerDetailRequest")
    @ResponsePayload //converte a resposta java em xml
    public GetCustomerDetailResponse processCustomerDetailRequest(@RequestPayload GetCustomerDetailRequest req) throws Exception {
       Customer customer = service.findById(req.getId());
       if(customer == null){
           throw new CustormerNotFoundException("Invalid Customer ID " +req.getId());
       }
 
        return convertToGetCustomerDetailResponse(customer);
    }

    private GetCustomerDetailResponse convertToGetCustomerDetailResponse(Customer customer){
        GetCustomerDetailResponse resp = new GetCustomerDetailResponse();
        resp.setCustomerDetail(convertToCustomerDetail(customer));
        return resp;
    }

    private CustomerDetail convertToCustomerDetail(Customer customer){
        CustomerDetail cd = new CustomerDetail();
        cd.setId(customer.getId());
        cd.setName(customer.getName());
        cd.setPhone(customer.getPhone());
        cd.setEmail(customer.getEmail());
        return cd;
    }

    @PayloadRoot(namespace = "http://klebercardoso.com", localPart = "GetAllCustomerDetailRequest")
    @ResponsePayload //converte a resposta java em xml
    public GetAllCustomerDetailResponse processGetAllCustomerDetailRequest(@RequestPayload GetAllCustomerDetailRequest req) throws Exception {
        List<Customer> customers = service.findAll();
        return convertToGetAllCustomerDetailResponse(customers);
    }

    private GetAllCustomerDetailResponse convertToGetAllCustomerDetailResponse(List<Customer> customers){
        GetAllCustomerDetailResponse resp = new GetAllCustomerDetailResponse();
        customers.stream().forEach(c -> resp.getCustomerDetail().add(convertToCustomerDetail(c)));
        return resp;
    }

    @PayloadRoot(namespace = "http://klebercardoso.com", localPart = "DeleteCustomerRequest")
    @ResponsePayload //converte a resposta java em xml
    public DeleteCustomerResponse deleteCustomerRequest(@RequestPayload DeleteCustomerRequest req) throws Exception {
        DeleteCustomerResponse resp = new DeleteCustomerResponse();
        resp.setStatus(convertStatusSoap(service.deleteById(req.getId())));
        return resp;
    }


    private com.klebercardoso.Status convertStatusSoap(com.kleber.webservices.customersadministration.helper.Status statusService){
        if(statusService == com.kleber.webservices.customersadministration.helper.Status.FAILURE)
            return com.klebercardoso.Status.FAILURE;
        
        return com.klebercardoso.Status.SUCCESS;
    }

    @PayloadRoot(namespace = "http://klebercardoso.com", localPart = "InsertCustomerRequest")
    @ResponsePayload //converte a resposta java em xml
    public InsertCustomerResponse insertCustomerRequest(@RequestPayload InsertCustomerRequest req) throws Exception {
        InsertCustomerResponse resp = new InsertCustomerResponse();
        resp.setStatus(convertStatusSoap(service.insert(convertCustomer(req.getCustomerDetail()))));
        return resp;
    }

    private Customer convertCustomer(CustomerDetail customerDetail){

        return new Customer(
            customerDetail.getId(), 
            customerDetail.getName(),
            customerDetail.getPhone(),
            customerDetail.getEmail()
            );
    }

}